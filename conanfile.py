# Copied from https://github.com/lasote/conan-poco
# Modified default options and only copies header folders actually configured
from conans import ConanFile
from conans.tools import download, unzip, replace_in_file
import os
import shutil
from conans import CMake


class PocoConan(ConanFile):
    name = "Poco"
    version = "1.7.6"
    url="https://bitbucket.org/lbakman/conan-poco.git"
    exports = "CMakeLists.txt"
    generators = "cmake", "txt"
    settings = "os", "arch", "compiler", "build_type"
    options = {"shared": [True, False],
               "enable_xml": [True, False],
               "enable_json": [True, False],
               "enable_mongodb": [True, False],
               "enable_pdf": [True, False],
               "enable_util": [True, False],
               "enable_net": [True, False],
               "enable_netssl": [True, False],
               "enable_netssl_win": [True, False],
               "enable_crypto": [True, False],
               "enable_data": [True, False],
               "enable_data_sqlite": [True, False],
               "enable_data_mysql": [True, False],
               "enable_data_odbc": [True, False],
               "enable_sevenzip": [True, False],
               "enable_zip": [True, False],
               "enable_apacheconnector": [True, False],
               "enable_cppparser": [True, False],
               "enable_pocodoc": [True, False],
               "enable_pagecompiler": [True, False],
               "enable_pagecompiler_file2page": [True, False],
               "force_openssl": [True, False], #  "Force usage of OpenSSL even under windows"
               "enable_tests": [True, False],
               "poco_unbundled": [True, False]
               }
    default_options = '''
shared=False
enable_xml=False
enable_json=False
enable_mongodb=False
enable_pdf=False
enable_util=True
enable_net=True
enable_netssl=False
enable_netssl_win=False
enable_crypto=False
enable_data=False
enable_data_sqlite=False
enable_data_mysql=False
enable_data_odbc=False
enable_sevenzip=False
enable_zip=False
enable_apacheconnector=False
enable_cppparser=False
enable_pocodoc=False
enable_pagecompiler=False
enable_pagecompiler_file2page=False
force_openssl=False
enable_tests=False
poco_unbundled=False
'''

    def source(self):
        zip_name = "poco-%s-release.zip" % self.version
        download("https://github.com/pocoproject/poco/archive/%s" % zip_name, zip_name)
        unzip(zip_name)
        shutil.move("poco-poco-%s-release" % self.version, "poco")
        os.unlink(zip_name)
        shutil.move("poco/CMakeLists.txt", "poco/CMakeListsOriginal.cmake")
        shutil.move("CMakeLists.txt", "poco/CMakeLists.txt")

    def config(self):
        if self.options.enable_netssl or self.options.enable_crypto or self.options.force_openssl:
            # self.output.warn("ENABLED OPENSSL DEPENDENCY!!")
            self.requires.add("OpenSSL/1.0.2i@lasote/stable", private=False)
            self.options["OpenSSL"].shared = self.options.shared
            if self.options.shared and self.settings.compiler == "apple-clang" \
                and self.settings.compiler.version == "7.3":
                self.options["OpenSSL"].shared = False
        else:
            if "OpenSSL" in self.requires:
                del self.requires["OpenSSL"]

        if self.options.enable_data_mysql:
            self.requires.add("MySQLClient/6.1.6@hklabbers/stable")
        else:
            if "MySQLClient" in self.requires:
                del self.requires["MySQLClient"]

    def build(self):
        cmake = CMake(self.settings)
        # Wrap original CMakeLists.txt for be able to include and call CONAN_BASIC_SETUP
        # It will allow us to set architecture flags, link with the requires etc
        cmake_options = []
        for option_name in self.options.values.fields:
            activated = getattr(self.options, option_name)
            the_option = "%s=" % option_name.upper()
            if option_name == "shared":
                the_option = "POCO_STATIC=OFF" if activated else "POCO_STATIC=ON"
                if not activated:
                    cmake_options.append("CMAKE_POSITION_INDEPENDENT_CODE=ON")
            else:
                the_option += "ON" if activated else "OFF"
            cmake_options.append(the_option)

        options_poco = " -D".join(cmake_options)

        if self.settings.compiler == "Visual Studio":
            if self.settings.compiler.runtime == "MT" or self.settings.compiler.runtime == "MTd":
                options_poco += " -DPOCO_MT=ON"
            else:
                options_poco += " -DPOCO_MT=OFF"

        self.run('cd poco && cmake . %s -D%s' % (cmake.command_line, options_poco))
        self.run("cd poco && cmake --build . %s" % cmake.build_config)

    def package(self):
        """ Copy required headers, libs and shared libs from the build folder to the package
        """
        # Typically includes we want to keep_path=True (default)
        headers = [("enable_util", "Util"),
                   ("enable_xml", "XML"),
                   ("enable_json", "JSON"),
                   ("enable_mongodb", "MongoDB"),
                   ("enable_pdf", "PDF"),
                   ("enable_net", "Net"),
                   ("enable_netssl", "NetSSL_OpenSSL"),
                   ("enable_netssl_win", "NetSSL_Win"),
                   ("enable_crypto", "Crypto"),
                   ("enable_data", "Data"),
                   ("enable_data_sqlite", "Data/SQLite"),
                   ("enable_data_mysql", "Data/MySQL"),
                   ("enable_data_odbc", "Data/ODBC"),
                   ("enable_sevenzip", "SevenZip"),
                   ("enable_zip", "Zip"),
                   ("enable_apacheconnector", "ApacheConnector")]
        for flag, header in headers:
            if getattr(self.options, flag):
                self.copy(pattern="*.h", dst="include", src="poco/%s/include" % header)

        for header in ["Foundation"]:
            self.copy(pattern="*.h", dst="include", src="poco/%s/include" % header)

        # But for libs and dlls, we want to avoid intermediate folders
        self.copy(pattern="*.lib", dst="lib", src="poco/lib", keep_path=False)
        self.copy(pattern="*.a",   dst="lib", src="poco/lib", keep_path=False)
        self.copy(pattern="*.dll", dst="bin", src="poco/bin", keep_path=False)
        # in linux shared libs are in lib, not bin
        self.copy(pattern="*.so*", dst="lib", src="poco/lib", keep_path=False)
        self.copy(pattern="*.dylib", dst="lib", src="poco/lib", keep_path=False)

    def package_info(self):
        """ Define the required info that the consumers/users of this package will have
        to add to their projects
        """
        libs = [("enable_util", "PocoUtil"),
                ("enable_xml", "PocoXML"),
                ("enable_json", "PocoJSON"),
                ("enable_mongodb", "PocoMongoDB"),
                ("enable_pdf", "PocoPDF"),
                ("enable_net", "PocoNet"),
                ("enable_netssl", "PocoNetSSL"),
                ("enable_netssl_win", "PocoNetSSL_Win"),
                ("enable_crypto", "PocoCrypto"),
                ("enable_data", "PocoData"),
                ("enable_data_sqlite", "PocoDataSQLite"),
                ("enable_data_mysql", "PocoDataMySQL"),
                ("enable_data_odbc", "PocoDataODBC"),
                ("enable_sevenzip", "PocoSevenZip"),
                ("enable_zip", "PocoZip"),
                ("enable_apacheconnector", "PocoApacheConnector")]
        for flag, lib in libs:
            if getattr(self.options, flag):
                self.cpp_info.libs.append(lib)

        self.cpp_info.libs.append("PocoFoundation")

        if self.settings.compiler == "Visual Studio" and self.options.shared == False:
            if self.settings.compiler.runtime == "MT" or self.settings.compiler.runtime == "MTd":
                self.cpp_info.libs = ["%smt" % lib for lib in self.cpp_info.libs]
            else:
                self.cpp_info.libs = ["%smd" % lib for lib in self.cpp_info.libs]

        # Debug library names has "d" at the final
        if self.settings.build_type == "Debug":
            self.cpp_info.libs = ["%sd" % lib for lib in self.cpp_info.libs]

        # in linux we need to link also with these libs
        if self.settings.os == "Linux":
            self.cpp_info.libs.extend(["pthread", "dl", "rt"])

        if not self.options.shared:
            self.cpp_info.defines.extend(["POCO_STATIC=ON", "POCO_NO_AUTOMATIC_LIBS"])
            if self.settings.compiler == "Visual Studio":
                self.cpp_info.libs.extend(["ws2_32", "Iphlpapi.lib"])
